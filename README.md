# 🪣 Dreamwall
Dreamwall is a web application that helps everyone who needs to know how much paint they need to paint their 4 walled room.

Are you tired of buying too much paint and having to store then away in some corner of your house?

Or did it happen to you of buying less paint then you needed so you had to go again to the store to buy some more?

It doesn't need to be like that anymore! Let Dreamwall take care of all the math you need to not waste your money and time when you need to paint your walls.

Welcome to Dreamwall! 🪣

## 📦 Pre-requisites
- [Node.js](https://nodejs.org/en/)
- [Docker](https://www.docker.com/)
- [Docker compose](https://docs.docker.com/compose/)

## 🌳 Environment Variables
To run this project, you will need to add the following environment variables to your .env file

| Name                          | Description                         | Default Value                                  |
| ----------------------------- | ------------------------------------| -----------------------------------------------|
|APP_SECRET           | Will be used for encryption            | A0D7F8Gha8eung9A8EBV8ubnefq      |
|APP_WEB_URL           | Front-end URL            | http://localhost:3000      |
|MAIL_DRIVER           | Mail Driver            | ethereal      |
|SMS_DRIVER           | SMS Driver            | sendpulse      |
|STORAGE_DRIVER           | Storage Driver            | disk      |
|SENDPULSE_API_USER_ID           | SendPulse API User ID            |       |
|SENDPULSE_API_SECRET           | API Secret from SendPulse            |       |
|SENDPULSE_TOKEN_STORAGE           | SendPulse Token Storage Directory            |       |
|TWILIO_ACCOUNT_SID           | Twilio Account SID            |       |
|TWILIO_AUTH_TOKEN           | Twilio Auth Token            |       |
|AWS_ACCESS_KEY_ID           | AWS Access Key            |       |
|AWS_SECRET_ACCESS_KEY           | AWS Secret Key            |       |

##### More on SendPulse API [here](https://sendpulse.com/integrations/api)
## 💻 Getting started
- Clone the repository
```sh
git clone git@gitlab.com:antoniobfm/dreamwall-node.git
```

- Go into the project

```sh
cd dreamwall-node
```

- Create and populate ```.env``` (you can use ```.env.example``` as base)

- Create and populate ```ormconfig.json``` (you can use ```ormconfig.example.json``` as base)

- Run the project
```sh
docker-compose up
```

## ⚙️ Features
- Specify the height and width of your walls;
- Add windows and doors to each wall;
- Get the amount of paint you need to cover all your walls.

## 💼 Business Requirements
1. No wall can have less than 1 meter square or more than 50 meters square of area, but they can have different heights and widths
2. The total area of the doors and windows have to be less than 50% of the area of its wall
3. The height of walls with doors shall be, at least, 30 centimeter taller than the height of the door
4. Each window has these measurements: 2,00 x 1,20 meters
5. Each door has these measurements: 0,80 x 1,90 meters
6. Each liter of paint is capable of covering 5 meters square
7. Do not consider ceiling and floor
8. The variations of the cans of paint are:
   - 0,5 L
   - 2,5 L
   - 3,6 L
   - 18 L



## 📚 Tech Stack

**Server:** Node.js, Express and TypeORM

**Database:** PostgreSQL


## 📄 License
Licensed under the MIT License, Copyright © 2022-present Antônio Moraes.

See LICENSE for more information
