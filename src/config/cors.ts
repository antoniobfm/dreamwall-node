var whitelist = ['http://localhost:3000', 'https://qualaboa.app', 'https://api.mercadopago.com']

const CORS_OPTIONS = {
	// origin: function (origin, callback) {
  //   if (whitelist.indexOf(origin) !== -1) {
  //     callback(null, true)
  //   } else {
  //     callback(new Error('Not allowed by CORS'))
  //   }
  // },
	credentials: true,
};

export default CORS_OPTIONS;
