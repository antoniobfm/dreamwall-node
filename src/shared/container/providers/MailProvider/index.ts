import { container } from 'tsyringe';

import mailConfig from '@config/mail';

import EtherealMailProvider from './implementations/EtherealMailProvider';
import SESMailProvider from './implementations/SESMailProvider';
import SendgridMailProvider from './implementations/SendgridMailProvider';

import IMailProvider from './models/IMailProvider';

const providers = {
	ethereal: container.resolve(EtherealMailProvider),
	ses: container.resolve(SESMailProvider),
	sendgrid: container.resolve(SendgridMailProvider)
};

container.registerInstance<IMailProvider>(
	'MailProvider',
	providers[mailConfig.driver],
);
