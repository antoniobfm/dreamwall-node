
import { ICreateRoom } from '@modules/rooms/dtos/ICreateRoom';
import { IRoomsRepository } from '@modules/rooms/repositories/IRoomsRepository';
import { getRepository, Repository } from 'typeorm';

import { Rooms } from '../entities/Rooms';

class RoomsRepository implements IRoomsRepository {
	private ormRepository: Repository<Rooms>;

	constructor() {
		this.ormRepository = getRepository(Rooms);
	}

	public async findById(id: string): Promise<Rooms | undefined> {
		const rooms = await this.ormRepository.findOne(id, {
			relations: ['walls'],
		});

		return rooms;
	}

	public async create(data: ICreateRoom): Promise<Rooms> {
		const room = this.ormRepository.create(
			data
		);

		await this.ormRepository.save(room);

		return room;
	}
}

export default RoomsRepository;
