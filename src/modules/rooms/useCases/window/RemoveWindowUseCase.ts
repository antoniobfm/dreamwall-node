import { injectable, inject } from 'tsyringe';

import {AppError} from '@shared/errors/AppError';

import { IWallsRepository } from '@modules/rooms/repositories/IWallsRepository';
import { Walls } from '@modules/rooms/infra/typeorm/entities/Walls';

@injectable()
class RemoveWindowUseCase {
	constructor(
		@inject('WallsRepository')
		private wallsRepository: IWallsRepository,
	) {}

	public async execute(id: string): Promise<Walls> {
		const wall = await this.wallsRepository.findById(id);

		if (!wall) {
			throw new AppError('Wall not found');
		}

		if (wall.windows <= 0) {
			throw new AppError('Wall cannot contain less than 0 windows');
		}

		const newWall = { ...wall, windows: wall.windows - 1 };

		await this.wallsRepository.save(newWall);

		return newWall;
	}
}

export default RemoveWindowUseCase;
