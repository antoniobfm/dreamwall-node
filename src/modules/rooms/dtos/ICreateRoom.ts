interface Wall {
	width: number;
	height: number;
}

export interface ICreateRoom {
	name: string;
	walls: Wall[];
}
