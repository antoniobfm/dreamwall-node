import { Request, Response } from 'express';
import { container } from 'tsyringe';
import AddDoorUseCase from './AddDoorUseCase';
import RemoveDoorUseCase from './RemoveDoorUseCase';


class DoorController {

	public async handle(
		request: Request,
		response: Response,
	): Promise<Response> {
		const { id, method } = request.body;

		let doorUseCase;

		if (method === 'add') {
			doorUseCase = container.resolve(AddDoorUseCase);
		} else {
			doorUseCase = container.resolve(RemoveDoorUseCase);
		}

		const wall = await doorUseCase.execute(id);

		return response.json(wall);
	}
}

export { DoorController }
