interface IMailConfig {
	driver: 'ethereal' | 'ses' | 'sendgrid';

	defaults: {
		from: {
			email: string;
			name: string;
		};
	};
}

export default {
	driver: process.env.MAIL_DRIVER || 'ethereal',

	defaults: {
		from: {
			email: 'pedrodosuporte@qualaboa.app',
			name: 'Pedro do Suporte',
		},
	},
} as IMailConfig;
