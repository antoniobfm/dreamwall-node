import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	JoinColumn,
	ManyToOne,
} from 'typeorm';

import { Exclude } from 'class-transformer';
import { Rooms } from './Rooms';

@Entity('walls')
class Walls {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column('int')
	width: number;

	@Column('int')
	height: number;

	@Column('uuid')
	room_id: string;

	@Column('int', { default: 0})
	windows: number;

	@Column('int', { default: 0})
	doors: number;

	@ManyToOne(() => Rooms, room => room.walls)
	@JoinColumn({ name: 'room_id' })
	room: Rooms;

	@CreateDateColumn()
	@Exclude()
	created_at: Date;

	@UpdateDateColumn()
	@Exclude()
	updated_at: Date;
}

export { Walls };
