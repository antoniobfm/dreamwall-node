import { utcToZonedTime } from "date-fns-tz"

export function timeZoneToSaoPaulo(time: string | Date): Date {
	const timeZone = 'America/Sao_Paulo'
	const zonedDate = utcToZonedTime(time, timeZone)

	return zonedDate
}
