export default {
	secret_refresh_token: 'cfe275a59555b5653388e0b03c2d6cc',
	secret_token: 'cfe275a5908b5650488e0b0342c2d6cc',
	expires_in_token: '15m',
	expires_in_refresh_token: '30d',
	expires_refresh_token_days: 30,
	jwt: {
		secret: process.env.APP_SECRET || 'default',
		expiresIn: '1d',
	},
	hotmart: {
		hottok: process.env.HOTTOK_TOKEN || 'none',
	},
};
