interface IStorageProvider {
    save(file: string, file_mimetype: string, folder: string): Promise<string>;
    delete(file: string, folder: string): Promise<void>;
}

export { IStorageProvider };
