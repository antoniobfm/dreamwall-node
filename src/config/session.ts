import { SessionOptions } from 'express-session';

const ONE_HOUR = 1000 * 60 * 60;

// const THIRTY_MINUTES = ONE_HOUR / 2;

const SIX_HOURS = ONE_HOUR * 6;

const THREE_DAYS = SIX_HOURS * 12;

const { env } = process;

export const {
	SESSION_SECRET = env.APP_SECRET || 'AUNasisuHD8Q7gf872g37rg2378gwd8Fb88g3r',
	SESSION_NAME = 'sid',
	SESSION_IDLE_TIMEOUT = THREE_DAYS,
	APP_WEB_URL,
	APP_WEB_DOMAIN,
} = env;

export const SESSION_ABSOLUTE_TIMEOUT = +(
	env.SESSION_ABSOLUTE_TIMEOUT || THREE_DAYS
);

export const SESSION_OPTIONS: SessionOptions = {
	secret: SESSION_SECRET,
	name: SESSION_NAME,
	proxy: true,
	cookie: {
		maxAge: +SESSION_IDLE_TIMEOUT,
		secure: false, // Change to TRUE in production
	},
};
