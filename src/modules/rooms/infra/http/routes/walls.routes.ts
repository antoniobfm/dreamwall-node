import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';
import { DoorController } from '@modules/rooms/useCases/door/DoorController';
import { WindowController } from '@modules/rooms/useCases/window/WindowController';


const wallsRouter = Router();

const windowController = new WindowController();
const doorController = new DoorController();

wallsRouter.post(
	'/window',
	celebrate({
		[Segments.BODY]: {
			id: Joi.string().required(),
			method: Joi.string().required(),
		},
	}),
	windowController.handle,
);

wallsRouter.post(
	'/door',
	celebrate({
		[Segments.BODY]: {
			id: Joi.string().required(),
			method: Joi.string().required(),
		},
	}),
	doorController.handle,
);

export default wallsRouter;
