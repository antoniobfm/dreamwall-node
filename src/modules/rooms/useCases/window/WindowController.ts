import { Request, Response } from 'express';
import { container } from 'tsyringe';
import AddWindowUseCase from './AddWindowUseCase';
import RemoveWindowUseCase from './RemoveWindowUseCase';


class WindowController {

	public async handle(
		request: Request,
		response: Response,
	): Promise<Response> {
		const { id, method } = request.body;

		let windowUseCase;

		if (method === 'add') {
			windowUseCase = container.resolve(AddWindowUseCase);
		} else {
			windowUseCase = container.resolve(RemoveWindowUseCase);
		}

		const wall = await windowUseCase.execute(id);

		return response.json(wall);
	}
}

export { WindowController }
