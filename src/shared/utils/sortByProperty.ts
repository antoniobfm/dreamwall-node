const byProperty = function (prop) {
	return function (a, b) {
		if (typeof a[prop] === 'number') {
			return a[prop] - b[prop];
		}
		return a[prop] < b[prop] ? -1 : a[prop] > b[prop] ? 1 : 0;
	};
};

export default byProperty;
