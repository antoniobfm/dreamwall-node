import ISendMailDTO from '../dtos/ISendMailDTO';
import ISendPasswordRecoveryDTO from '../dtos/ISendPasswordRecoveryDTO';
import ISendVerificationCodeDTO from '../dtos/ISendVerificationCodeDTO';

export default interface IMailProvider {
	sendMail(data: ISendMailDTO): Promise<void>;
	sendVerificationCode(data: ISendVerificationCodeDTO): Promise<void>;
	sendPasswordRecovery(data: ISendPasswordRecoveryDTO): Promise<void>;
}
