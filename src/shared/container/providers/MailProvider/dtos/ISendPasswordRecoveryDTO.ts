interface IMailContact {
	name: string;
	email: string;
}

export default interface ISendPasswordRecoveryDTO {
	to: IMailContact;
	verification_code: string
}
