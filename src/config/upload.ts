import crypto from 'crypto';
import multer from 'multer';
import { resolve } from 'path';

const tmpFolder = resolve(__dirname, '..', '..', 'tmp');
const takeSpaceOut = /\s[^A-Za-z0-9\\s]*/g;

export default {
	tmpFolder,
	storage: multer.diskStorage({
		destination: tmpFolder,
		filename: (request, file, callback) => {
			const fileHash = crypto.randomBytes(16).toString('hex');
			const fileName = `${fileHash}-${file.originalname.replace(takeSpaceOut, '')}`;

			return callback(null, fileName);
		},
	}),
};
