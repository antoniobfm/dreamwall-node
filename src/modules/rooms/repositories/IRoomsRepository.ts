import { ICreateRoom } from '../dtos/ICreateRoom';
import { Rooms } from '../infra/typeorm/entities/Rooms';

interface IRoomsRepository {
	create(data: ICreateRoom): Promise<Rooms>;
	findById(id: string): Promise<Rooms | undefined>;
}

export { IRoomsRepository };
