import { Transporter } from 'nodemailer/';
import mailConfig from '@config/mail';
import sgMail, { MailDataRequired } from '@sendgrid/mail'

import { injectable, inject } from 'tsyringe';

import IMailProvider from '../models/IMailProvider';
import ISendMailDTO from '../dtos/ISendMailDTO';
import ISendVerificationCodeDTO from '../dtos/ISendVerificationCodeDTO';
import ISendPasswordRecoveryDTO from '../dtos/ISendPasswordRecoveryDTO';

@injectable()
export default class SendgridMailProvider implements IMailProvider {
	private client: Transporter;


	constructor(
	) {
		sgMail.setApiKey(process.env.SENDGRID_API_KEY);
	}

	public async sendMail({
		to,
		subject,
		from,
		// templateData,
	}: ISendMailDTO): Promise<void> {
	const msg: MailDataRequired = {
		to,
		from: 'pedrodosuporte@qualaboa.app', // Use the email address or domain you verified above
		subject: 'Sending with Twilio SendGrid is Fun',
		templateId: 'd-5bea1a9803404bac980b2ae93ab844e6',
		dynamicTemplateData: {
			verification_code: '0000000'
		}
	};

	sgMail
  .send(msg)
  .then(() => {
    console.log('Email sent')
  })
  .catch((error) => {
    console.error(error)
  })
	}

	public async sendVerificationCode({
		to,
		verification_code
	}: ISendVerificationCodeDTO): Promise<void> {
	const msg: MailDataRequired = {
		to,
		from: 'pedrodosuporte@qualaboa.app', // Use the email address or domain you verified above
		subject: 'Qual a boa - Recuperação de senha',
		templateId: 'd-5bea1a9803404bac980b2ae93ab844e6',
		dynamicTemplateData: {
			verification_code: verification_code
		}
	};

	sgMail
  .send(msg)
  .then(() => {
    console.log('Email sent')
  })
  .catch((error) => {
    console.error(error)
  })
	}

	public async sendPasswordRecovery({
		to,
		verification_code
	}: ISendPasswordRecoveryDTO): Promise<void> {
	const msg: MailDataRequired = {
		to,
		from: 'pedrodosuporte@qualaboa.app', // Use the email address or domain you verified above
		subject: 'Qual a boa - Recuperação de senha',
		templateId: 'd-5bea1a9803404bac980b2ae93ab844e6',
		dynamicTemplateData: {
			verification_code: verification_code
		}
	};

	sgMail
  .send(msg)
  .then(() => {
    console.log('Email sent')
  })
  .catch((error) => {
    console.error(error)
  })
	}
}
