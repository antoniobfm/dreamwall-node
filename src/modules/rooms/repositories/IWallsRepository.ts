import { Walls } from '../infra/typeorm/entities/Walls';

interface IWallsRepository {
	findById(id: string): Promise<Walls | undefined>;
	save(data: Walls): Promise<Walls>;
}

export { IWallsRepository };
