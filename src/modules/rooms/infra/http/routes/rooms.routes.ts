import { Router } from 'express';
import { celebrate, Segments, Joi } from 'celebrate';
import { DoorController } from '@modules/rooms/useCases/door/DoorController';
import { WindowController } from '@modules/rooms/useCases/window/WindowController';
import { CreateRoomController } from '@modules/rooms/useCases/createRoom/CreateRoomController';
import { ShowRoomController } from '@modules/rooms/useCases/showRoom/ShowRoomController';
import { AmountOfPaintNeededController } from '@modules/rooms/useCases/amountOfPaintNeeded/AmountOfPaintNeededController';


const roomsRouter = Router();

const createRoomController = new CreateRoomController();
const showRoomController = new ShowRoomController();
const amountOfPaintNeededController = new AmountOfPaintNeededController();

roomsRouter.post(
	'/create',
	celebrate({
		[Segments.BODY]: {
			name: Joi.string().required().messages({'required': 'Name is required'}),
			walls: Joi.array().items({
				width: Joi.number().required().messages({'required': 'Walls width is required'}),
				height: Joi.number().required().messages({'required': 'Walls height is required'}),
			}).required().messages({'required': 'Walls is required'}),
		},
	}),
	createRoomController.handle,
);

roomsRouter.post(
	'/show',
	celebrate({
		[Segments.BODY]: {
			id: Joi.string().required().messages({'required': 'Room id is required'}),
		},
	}),
	showRoomController.handle,
);

roomsRouter.post(
	'/how-much-paint-do-i-need',
	celebrate({
		[Segments.BODY]: {
			id: Joi.string().required().messages({'required': 'Room id is required'}),
		},
	}),
	amountOfPaintNeededController.handle,
);

export default roomsRouter;
