import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateWalls1662046337679 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.createTable(
			new Table({
				name: 'walls',
				columns: [
					{
						name: 'id',
						type: 'uuid',
						isPrimary: true,
						generationStrategy: 'uuid',
						default: 'uuid_generate_v4()',
					},
					{
						name: 'width',
						type: 'int',
					},
					{
						name: 'height',
						type: 'int',
					},
					{
						name: 'windows',
						type: 'int',
						default: 0,
					},
					{
						name: 'doors',
						type: 'int',
						default: 0,
					},
					{
						name: 'created_at',
						type: 'timestamp',
						default: 'now()',
					},
					{
						name: 'updated_at',
						type: 'timestamp',
						default: 'now()',
					},
				],
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropTable('walls');
	}
}
