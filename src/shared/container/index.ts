import { container } from 'tsyringe';

import './providers';

import { IRoomsRepository } from '@modules/rooms/repositories/IRoomsRepository';
import RoomsRepository from '@modules/rooms/infra/typeorm/repositories/RoomsRepository';

import { IWallsRepository } from '@modules/rooms/repositories/IWallsRepository';
import WallsRepository from '@modules/rooms/infra/typeorm/repositories/WallsRepository';

container.registerSingleton<IRoomsRepository>(
	'RoomsRepository',
	RoomsRepository,
);

container.registerSingleton<IWallsRepository>(
	'WallsRepository',
	WallsRepository,
);



