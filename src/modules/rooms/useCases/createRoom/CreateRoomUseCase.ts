import { injectable, inject } from 'tsyringe';

import {AppError} from '@shared/errors/AppError';

import { ICreateRoom } from '@modules/rooms/dtos/ICreateRoom';
import { Rooms } from '@modules/rooms/infra/typeorm/entities/Rooms';
import { IRoomsRepository } from '@modules/rooms/repositories/IRoomsRepository';

@injectable()
class CreateRoomUseCase {
	constructor(
		@inject('RoomsRepository')
		private roomsRepository: IRoomsRepository,
	) {}

	public async execute({
		name,
		walls
	}: ICreateRoom): Promise<Rooms> {

		walls.map(wall => {
			const area = wall.width * wall.height;

			if (area > 500000) {
				throw new AppError('Wall area should be less than 50m²');
			} else if (area < 10000) {
				throw new AppError('Wall area should be more than 1m²');
			}
		});

		const room = await this.roomsRepository.create({
			name,
			walls
		});

		return room;
	}
}

export default CreateRoomUseCase;
