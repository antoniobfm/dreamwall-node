import {MigrationInterface, QueryRunner, TableColumn, TableForeignKey} from "typeorm";

export class RoomsConnectionToWalls1662046420245 implements MigrationInterface {
	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.addColumn(
			'walls',
			new TableColumn({
				name: 'room_id',
				type: 'uuid',
			}),
		);

		await queryRunner.createForeignKey(
			'walls',
			new TableForeignKey({
				name: 'WallsRoom',
				columnNames: ['room_id'],
				referencedTableName: 'rooms',
				referencedColumnNames: ['id'],
				onDelete: 'SET NULL',
				onUpdate: 'SET NULL',
			}),
		);
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.dropForeignKey('walls', 'WallsRoom');

		await queryRunner.dropColumn('walls', 'room_id');
	}
}
