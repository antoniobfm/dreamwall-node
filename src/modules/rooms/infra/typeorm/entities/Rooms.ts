import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	CreateDateColumn,
	UpdateDateColumn,
	OneToMany,
	JoinColumn,
	ManyToOne,
} from 'typeorm';

import { Exclude } from 'class-transformer';
import { Walls } from './Walls';

@Entity('rooms')
class Rooms {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column('varchar')
	name: string;

	@OneToMany(() => Walls, walls => walls.room, {
		cascade: ['insert'],
	})
	walls: Walls[];

	@CreateDateColumn()
	@Exclude()
	created_at: Date;

	@UpdateDateColumn()
	@Exclude()
	updated_at: Date;
}

export { Rooms };
