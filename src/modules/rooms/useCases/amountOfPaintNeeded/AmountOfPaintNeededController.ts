import { Request, Response } from 'express';
import { container } from 'tsyringe';
import AmountOfPaintNeededUseCase from './AmountOfPaintNeededUseCase';


class AmountOfPaintNeededController {
	public async handle(
		request: Request,
		response: Response,
	): Promise<Response> {
		const { id } = request.body;

		const amountOfPaintNeededUseCase = container.resolve(AmountOfPaintNeededUseCase);

		const createRoom = await amountOfPaintNeededUseCase.execute(id);

		return response.json(createRoom);
	}
}

export { AmountOfPaintNeededController }
