import { Request, Response } from 'express';
import { container } from 'tsyringe';
import ShowRoomUseCase from './ShowRoomUseCase';


class ShowRoomController {

	public async handle(
		request: Request,
		response: Response,
	): Promise<Response> {
		const { id } = request.body;

		const showRoom = container.resolve(ShowRoomUseCase);

		const room = await showRoom.execute(id);

		return response.json(room);
	}
}

export { ShowRoomController }
