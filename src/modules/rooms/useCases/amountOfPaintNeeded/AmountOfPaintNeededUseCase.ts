import { injectable, inject } from 'tsyringe';

import {AppError} from '@shared/errors/AppError';

import { IRoomsRepository } from '@modules/rooms/repositories/IRoomsRepository';
import { cans, door, window } from '@modules/rooms/config/dimensions';

interface IResponse {
	18: number;
	3.6: number;
	2.5: number;
	0.5: number;
}

@injectable()
class AmountOfPaintNeededUseCase {
	constructor(
		@inject('RoomsRepository')
		private roomsRepository: IRoomsRepository,
	) {}

	public async execute(id: string): Promise<IResponse> {
		const room = await this.roomsRepository.findById(id);

		if (!room) {
			throw new AppError('Room not found');
		}

		const newCansNeeded = {
			18: 0,
			3.6: 0,
			2.5: 0,
			0.5: 0,
		};

		let paintLeft =
			room.walls.reduce((acc, wall) => {
				const wallArea = wall.height * wall.width;
				const windowArea = wall.windows * window.width * window.height;
				const doorArea = wall.doors * door.width * door.height;

				const needPaint = wallArea - (windowArea + doorArea);

				return acc + needPaint;
			}, 0) / 50000;

		for (let i = 0; cans.length > i; i++) {
			if (paintLeft === 0) {
				break;
			}

			const currentCan = cans[i];
			const notCoveredByThisCan = paintLeft % currentCan;
			const coveredByThisCan = paintLeft - notCoveredByThisCan;
			paintLeft = notCoveredByThisCan;

			const cansOfThisType = coveredByThisCan / currentCan;

			newCansNeeded[`${currentCan}`] = cansOfThisType;
		}

		if (paintLeft > 0) {
			newCansNeeded['0.5'] += 1;

			if (newCansNeeded['0.5'] === 5) {
				newCansNeeded['2.5'] += 1;
				newCansNeeded['0.5'] = 0;
			}
		}

		return newCansNeeded;
	}
}

export default AmountOfPaintNeededUseCase;
