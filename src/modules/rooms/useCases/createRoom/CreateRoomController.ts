import { Request, Response } from 'express';
import { container } from 'tsyringe';
import CreateRoomUseCase from './CreateRoomUseCase';


class CreateRoomController {

	public async handle(
		request: Request,
		response: Response,
	): Promise<Response> {
		const { name, walls } = request.body;

		const createRoomUseCase = container.resolve(CreateRoomUseCase);

		const createRoom = await createRoomUseCase.execute({
			name,
			walls
		});

		return response.json(createRoom);
	}
}

export { CreateRoomController }
