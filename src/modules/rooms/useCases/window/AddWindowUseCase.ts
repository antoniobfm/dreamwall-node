import { injectable, inject } from 'tsyringe';

import {AppError} from '@shared/errors/AppError';

import { IWallsRepository } from '@modules/rooms/repositories/IWallsRepository';
import { Walls } from '@modules/rooms/infra/typeorm/entities/Walls';
import { door, window } from '@modules/rooms/config/dimensions';

@injectable()
class AddWindowUseCase {
	constructor(
		@inject('WallsRepository')
		private wallsRepository: IWallsRepository,
	) {}

	public async execute(id: string): Promise<Walls> {
		const wall = await this.wallsRepository.findById(id);

		if (!wall) {
			throw new AppError('Wall not found');
		}

		const area = wall.width * wall.height;
		const windowsArea = (wall.windows + 1) * window.area;
		const doorsArea = wall.doors * door.area;

		if (area / 2 <= windowsArea + doorsArea) {
			throw new AppError('Wall area should be more than 50% of windows and doors area');
		}

		const newWall = { ...wall, windows: wall.windows + 1 };

		const room = await this.wallsRepository.save(newWall)

		return room;
	}
}

export default AddWindowUseCase;
