import { injectable, inject } from 'tsyringe';

import {AppError} from '@shared/errors/AppError';

import { Rooms } from '@modules/rooms/infra/typeorm/entities/Rooms';
import { IRoomsRepository } from '@modules/rooms/repositories/IRoomsRepository';

@injectable()
class ShowRoomUseCase {
	constructor(
		@inject('RoomsRepository')
		private roomsRepository: IRoomsRepository,
	) {}

	public async execute(id: string): Promise<Rooms> {
		const room = await this.roomsRepository.findById(id);

		if (!room) {
			throw new AppError('Room not found');
		}

		return room;
	}
}

export default ShowRoomUseCase;
