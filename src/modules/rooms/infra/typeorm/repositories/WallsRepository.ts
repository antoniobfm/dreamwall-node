import { IWallsRepository } from '@modules/rooms/repositories/IWallsRepository';
import { getRepository, Repository } from 'typeorm';

import { Walls } from '../entities/Walls';

class WallsRepository implements IWallsRepository {
	private ormRepository: Repository<Walls>;

	constructor() {
		this.ormRepository = getRepository(Walls);
	}

	public async findById(id: string): Promise<Walls | undefined> {
		const wall = await this.ormRepository.findOne(id);

		return wall;
	}

	public async save(data: Walls): Promise<Walls> {
		const wall = await this.ormRepository.save(data);

		return wall;
	}

}

export default WallsRepository;
