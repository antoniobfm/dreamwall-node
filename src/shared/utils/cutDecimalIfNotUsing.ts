import toFixedNumber from '@shared/utils/formatNumbers';

function cutDecimalIfNotUsing(number: number): number {
	return number % 1 === 0 ? parseInt(number, 10) : toFixedNumber(number, 2, 10);
}
export default cutDecimalIfNotUsing;
