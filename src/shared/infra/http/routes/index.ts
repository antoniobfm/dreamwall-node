import roomsRouter from '@modules/rooms/infra/http/routes/rooms.routes';
import wallsRouter from '@modules/rooms/infra/http/routes/walls.routes';
import { Router } from 'express';


const routes = Router();

routes.use('/rooms', roomsRouter);
routes.use('/walls', wallsRouter);

export default routes;
