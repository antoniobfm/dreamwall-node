interface IMailContact {
	name: string;
	email: string;
}

export default interface ISendVerificationCodeDTO {
	to: IMailContact;
	verification_code: string
}
